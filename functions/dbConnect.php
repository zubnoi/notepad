<?php
function dbConnect()
{
    $opt = [
    PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
    PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
    PDO::ATTR_EMULATE_PREPARES => false
    ];
    $pdo = new PDO('mysql:host=localhost;charset=utf8;dbname=notebook', 'root', '', $opt);
    return $pdo;
}